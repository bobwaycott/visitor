# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

# import environment-specific config
import_config "#{Mix.env()}.secret.exs"
