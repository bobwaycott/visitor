# Visitor

`Visitor` is a simple Elixir library that anonymizes an IP address into a Murmur-hashed value, providing a way to uniquely identify visitors in a (hopefully) GDPR-compliant fashion that does not directly map back to any identifiable individual&mdash;because a Murmur-hash is non-reversible, and the only way to get the same results is to know the `:secret`, the implementation, and then process every possible IP address to find the results.

This isn't intended to be cryptographically secure.

## Installation

`Visitor` can be installed by adding `visitor` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:visitor, git: "git@gitlab.com:bobwaycott/visitor.git", branch: "master"}
  ]
end
```

## Usage

Using `Visitor` should be pretty straightforward. In config secrets, you'll need to add a `:secret` key. **This must be an `integer`.**

```elixir
config :visitor, secret: 123_456_789
```

Then, anonymizing a visitor should be as simple as:

```elixir
# assuming you have a `conn` request:
{:ok, visitor} = Visitor.anonymize(conn.remote_ip)

# using a string IP address in an `ip` variable:
{:ok, visitor} = Visitor.anonymize(ip)
```

You can also generate a random `Visitor` like so:

```elixir
{:ok, visitor} = Visitor.random
```

**NOTE:** `Visitor` is intended to be included in multiple projects that might want to share knowledge about visitors&mdash;for example, Site A & Site B are separate, but interested in knowing if they share visitors (well, as much as that can be guessed at based on IP address alone). This will only work if Site A & Site B are using the same `:secret` value. If the `Visitor :secret` isn't shared, `Visitor` will not return the same results.
