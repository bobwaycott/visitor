defmodule Guards do
  defmacro is_valid_tuple(ip) do
    quote do
      is_tuple(unquote(ip)) and tuple_size(unquote(ip)) in [4, 8]
    end
  end
end
