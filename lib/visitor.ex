defmodule Visitor do
  import Guards

  @secret Application.get_env(:visitor, :secret)
  @types [:ipv4, :ipv6]

  @doc """
  Anonymize an IP address to a (hopefully) unique identifier.
  """
  def anonymize(ip) when is_valid_tuple(ip) do
    res =
      ip
      |> tuple_to_ip
      |> hash

    {:ok, res}
  end

  def anonymize(ip) when is_binary(ip) do
    if is_valid?(ip) do
      {:ok, hash(ip)}
    else
      {:error, :invalid}
    end
  end

  def anonymize(_), do: {:error, :invalid}

  @doc """
  Returns a random anonymous visitor.
  """
  def random() do
    :random.seed(:os.timestamp())

    case @types |> Enum.random() do
      :ipv4 ->
        {rand4(), rand4(), rand4(), rand4()} |> anonymize

      :ipv6 ->
        {rand6(), rand6(), rand6(), rand6(), rand6(), rand6(), rand6(), rand6()} |> anonymize
    end
  end

  # Use Murmur hash to return 128-bit integer representation of `ip` address.
  # Returns a Base36-encoded identifer that can be stored in database.
  defp hash(ip) do
    ip
    |> Murmur.hash_x64_128(seed(ip))
    |> :erlang.integer_to_binary(36)
  end

  # Returns random integers in the IPv4 space
  defp rand4() do
    :random.uniform(255)
  end

  # Returns random integers in the IPv6 space
  defp rand6() do
    :random.uniform(65535)
  end

  # Generate a 32-bit integer seed from `ip` for use in hashing `ip`.
  # Doing this to hopefully never have any collisions.
  defp seed(ip) do
    ip |> Murmur.hash_x86_32(@secret)
  end

  # Convert an `ip` tuple into a properly formatted string of said `ip`.
  # This is hear because Phoenix app will be sending `conn.remote_ip`,
  # which is a tuple.
  # Supports IPv4 and IPv6 IPs.
  defp tuple_to_ip(ip) do
    ip
    |> :inet.ntoa()
    |> to_string
  end

  # Validates `ip` string passed in is a valid IPv4 or IPv6 address.
  # Uses Erlang's `:inet` module to parse the address string.
  defp is_valid?(ip) do
    case :inet.parse_address(String.to_charlist(ip)) do
      {:ok, _} -> true
      _ -> false
    end
  end
end
