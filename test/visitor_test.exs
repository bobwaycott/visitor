defmodule VisitorTest do
  use ExUnit.Case
  doctest Visitor

  test "anonymizing non-IP binary returns :error" do
    hash = Visitor.anonymize("foobar")
    assert hash == {:error, :invalid}
  end

  test "anonymizing IPv4 binary returns binary" do
    {:ok, hash} = Visitor.anonymize("192.168.1.234")
    assert is_binary(hash)
  end

  test "anonymizing IPv4 again returns same binary" do
    {:ok, hash} = Visitor.anonymize("192.168.1.234")
    {:ok, hash2} = Visitor.anonymize("192.168.1.234")
    assert hash == hash2
  end

  test "anonymizing full IPv6 binary returns binary" do
    {:ok, hash} = Visitor.anonymize("2001:0db8:85a3:0000:0000:8a2e:0370:7334")
    assert is_binary(hash)
  end

  test "anonymizing full IPv6 again returns same binary" do
    {:ok, hash} = Visitor.anonymize("2001:0db8:85a3:0000:0000:8a2e:0370:7334")
    {:ok, hash2} = Visitor.anonymize("2001:0db8:85a3:0000:0000:8a2e:0370:7334")
    assert hash == hash2
  end

  test "anonymizing IPv6 short-form binary returns binary" do
    {:ok, hash} = Visitor.anonymize("2001:db8:85a3::8a2e:370:7334")
    assert is_binary(hash)
  end

  test "anonymizing short IPv6 again returns same binary" do
    {:ok, hash} = Visitor.anonymize("2001:db8:85a3::8a2e:370:7334")
    {:ok, hash2} = Visitor.anonymize("2001:db8:85a3::8a2e:370:7334")
    assert hash == hash2
  end

  test "anonymizing IPv4 tuple returns binary" do
    {:ok, hash} = Visitor.anonymize({10, 0, 0, 1})
    assert is_binary(hash)
  end

  test "anonymizing IPv4 tuple again returns same binary" do
    {:ok, hash} = Visitor.anonymize({10, 0, 0, 1})
    {:ok, hash2} = Visitor.anonymize({10, 0, 0, 1})
    assert hash == hash2
  end

  test "anonymizing IPv6 tuple returns binary" do
    {:ok, hash} = Visitor.anonymize({8193, 3512, 34211, 0, 0, 0, 35374, 880})
    assert is_binary(hash)
  end

  test "anonymizing IPv6 tuple again returns same binary" do
    {:ok, hash} = Visitor.anonymize({8193, 3512, 34211, 0, 0, 0, 35374, 880})
    {:ok, hash2} = Visitor.anonymize({8193, 3512, 34211, 0, 0, 0, 35374, 880})
    assert hash == hash2
  end

  test "anonymizing invalid tuple returns :error" do
    hash = Visitor.anonymize({10, 0, 0})
    assert hash == {:error, :invalid}
    hash = Visitor.anonymize({10, 0, 0, 4, 5})
    assert hash == {:error, :invalid}
  end

  test "anonymizing non-binary returns :error" do
    hash = Visitor.anonymize(234)
    assert hash == {:error, :invalid}
  end

  test "random returns a valid anonymized hash" do
    {:ok, hash} = Visitor.random()
    assert is_binary(hash)
  end
end
